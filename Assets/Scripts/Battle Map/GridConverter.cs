using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class GridConverter
    {
        private Vector2 offset;
        private float cellSize;
        private float height;

        public GridConverter(Vector3 offset, float cellSize)
        {
            this.offset = new Vector2(offset.x, offset.z);
            this.cellSize = cellSize;
            this.height = offset.y;
        }

        public Vector2Int ConvertToGrid(Vector3 worldPosition)
        {
            var planePosition = new Vector2(worldPosition.x, worldPosition.z);
            var localPosition = planePosition - offset;

            var fpoint = localPosition / cellSize;

            return new Vector2Int(Mathf.RoundToInt(fpoint.x), Mathf.RoundToInt(fpoint.y));
        }

        public Vector3 ConvertToWorld(Vector2Int gridPosition)
        {
            var planePosition = offset + (Vector2)gridPosition * cellSize;

            return new Vector3(planePosition.x, height, planePosition.y);
        }
    }

