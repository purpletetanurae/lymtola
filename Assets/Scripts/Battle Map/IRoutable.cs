using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRoutable
{
    Vector3 GetNextPoint(Vector3 currentPoint);
}
