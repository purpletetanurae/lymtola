using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

public class Game : MonoBehaviour
{
    //[SerializeField] private EnemyFactory _enemyFactory;

    private Vector2Int randomPosition
    {
        get
        {
            return new Vector2Int(Random.Range(0, gridSize.x), Random.Range(0, gridSize.x));
        }
    }


    public void OnCreate(CallbackContext context)
    {
        gridView.ChangeTile(randomPosition, Random.Range(0f, 1f));
    }

    [SerializeField] private GridView gridView;
    [SerializeField] private Vector2Int gridSize;

    private void Start()
    {
        gridView.CreateGrid(gridSize);
    }
}
