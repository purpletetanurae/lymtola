using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public static void FillDistanceMatrix(Vector2Int size, Vector2Int start, ref int[][][] matrix)
    {
        Queue<Vector2Int> bypassPoints = new Queue<Vector2Int>();
        HashSet<Vector2Int> passedPoints = new HashSet<Vector2Int>();
        Vector2Int currentPoint;
        Vector2Int nextPoint;
        Vector2Int[] directions = new Vector2Int[4]
        {
            Vector2Int.up,
            Vector2Int.right,
            Vector2Int.down,
            Vector2Int.left
        };

        bypassPoints.Enqueue(start);


        while (bypassPoints.Count > 0)
        {
            currentPoint = bypassPoints.Dequeue();

            if (currentPoint.x < 0 || currentPoint.x >= size.x)
            {
                continue;
            }
            if (currentPoint.y < 0 || currentPoint.y >= size.y)
            {
                continue;
            }

            foreach (var direction in directions)
            {
                nextPoint = currentPoint + direction;

                if (passedPoints.Contains(nextPoint))
                {
                    continue;
                }

                bypassPoints.Enqueue(nextPoint);
            }
        }
    }
}
