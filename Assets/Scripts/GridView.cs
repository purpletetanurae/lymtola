using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridView : MonoBehaviour
{
    [SerializeField] private TileView tilePrefab;

    private TileView[,] tiles;

    public void CreateGrid(Vector2Int size)
    {
        tiles = new TileView[size.x, size.y];
        TileView instantiated;

        for (var x = 0; x < size.x; x++)
        {
            for (var y = 0; y < size.y; y++)
            {
                instantiated = Instantiate(tilePrefab, transform);

                instantiated.Position = new Vector3(x, 0, y);

                tiles[x, y] = instantiated;
            }
        }
    }

    public void ChangeTile(Vector2Int position, float value)
    {
        tiles[position.x, position.y].ColorValue = value;
    }
}
