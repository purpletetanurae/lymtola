using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    private Node[] adjacents = new Node[4];
    private int[] goalDistances = new int[4];
    private float[] dangerDirections = new float[4];

    public int GetGoalDistance(CardinalDirection cardinalDirection)
    {
        return goalDistances[(int)cardinalDirection];
    }

    public float GetDanger(CardinalDirection cardinalDirection)
    {
        return dangerDirections[(int)cardinalDirection];
    }

    public Node GetAdjacent(CardinalDirection cardinalDirection)
    {
        return adjacents[(int)cardinalDirection];
    }

}
