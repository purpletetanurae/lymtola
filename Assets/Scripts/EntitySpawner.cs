using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour
{
    [SerializeField] private BattleMap battleMap;

    [SerializeField] private EntityController entityPrefab;

    [SerializeField] private List<Vector3> spawnPoints = new List<Vector3>();

    [SerializeField] private float delay = 3f;

    [SerializeField] private int maxEntity = 10;

    private int entityCount;

    private Vector3 RandomSpawnPosition => spawnPoints[Random.Range(0, spawnPoints.Count)];

    private void Start()
    {
        StartCoroutine(Spawning());
    }

    private void Spawn()
    {
        var entity = Instantiate<EntityController>(entityPrefab);

        entity.Routable = battleMap;

        entity.transform.position = RandomSpawnPosition;

        entityCount++;
    }

    private IEnumerator Spawning()
    {
        yield return new WaitForSeconds(delay);

        while (entityCount < maxEntity)
        {
            Spawn();

            yield return new WaitForSeconds(delay);
        }
    }
}
