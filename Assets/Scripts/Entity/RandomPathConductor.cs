using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPathConductor : MonoBehaviour
{
    private Vector3[] directions = new Vector3[4]
    {
        Vector3.forward,
        Vector3.right,
        Vector3.back,
        Vector3.left
    };

    private List<Vector3> path = new List<Vector3>();

    private int currentIndex = 0;

    private float cellSize = 2f;

    private TargetMover mover;

    private void Awake()
    {
        mover = GetComponent<TargetMover>();

        FillPath(100);
    }

    private void Start()
    {
        mover.UpdateTarget(path[0]);
    }

    private void OnEnable()
    {
        mover.TargetReached += OnTargetReached;
    }

    private void OnTargetReached()
    {
        if (currentIndex >= path.Count - 1)
        {
            return;
        }

        currentIndex++;
        mover.UpdateTarget(path[currentIndex]);
    }

    private void FillPath(int size)
    {
        var direction = 0;
        Vector3 position = transform.position;

        for (var i = 0; i < size; i++)
        {
            direction = GetRandomDirection(ReverseDirection(direction));

            position = GetNextPosition(position, direction);
            path.Add(position);
        }
    }

    private Vector3 GetNextPosition(Vector3 currentPosition, int direction)
    {
        return currentPosition + directions[direction] * cellSize;
    }

    private int GetRandomDirection(int excludedDirection)
    {
        var randomValue = Random.Range(0, 3);

        return randomValue + (randomValue >= excludedDirection ? 1 : 0);
    }

    private void OnDrawGizmos()
    {
        if (path.Count == 0)
        {
            return;
        }

        if (currentIndex > 0)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(path[currentIndex - 1], 0.2f);
        }

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(path[currentIndex], 0.2f);

        if (currentIndex >= path.Count - 1)
        {
            return;
        }

        Gizmos.color = Color.green;
        Gizmos.DrawSphere(path[currentIndex + 1], 0.2f);
    }

    private int ReverseDirection(int direction)
    {
        return (direction + 2) % 4;
    }
}
