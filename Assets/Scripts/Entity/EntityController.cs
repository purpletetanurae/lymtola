using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityController : MonoBehaviour
{
    public IRoutable Routable {private get; set;}

    private TargetMover mover;

    private void Awake()
    {
        mover = GetComponent<TargetMover>();
    }

    private void Start()
    {
        UpdateTargetPosition();
    }

    private void OnEnable()
    {
        mover.TargetReached += OnTargetReached;
    }
    
    private void OnDisable()
    {
        mover.TargetReached -= OnTargetReached;
    }

    private void OnTargetReached()
    {
        UpdateTargetPosition();
    }

    private void UpdateTargetPosition()
    {
        var nextPosition = Routable.GetNextPoint(transform.position);

        mover.UpdateTarget(nextPosition);
    }
}
